
import axios from 'axios'
import { useEffect, useState } from 'react'
import './UserList.css'

const UserList = (props) => {

    const getUsers = 'https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/users'
    const [userData, setUserData] = useState([])

    useEffect(() => {
        axios.get(getUsers).then(response => {
            let newUserData = []

            response.data.map(user => {
                newUserData.push({
                    userInfos: user.user_infos,
                    workData: user.work_area
                })
            })
            setUserData(newUserData)
        })
    }, [])

    return (
        <table>
            <thead>
                <tr>
                    <th>№</th>
                    <th>Full Name</th>
                    <th>Date of birth</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Company Name</th>
                    <th>Job Type</th>
                    <th>Experience</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {userData.map(user => {
                    <tr key={user.userInfos.email}>
                        <td></td>
                        <td>{`${user.userInfos.firstName} ${user.userInfos.lastName}`}</td>
                        <td>{user.userInfos.dob}</td>
                        <td>{user.userInfos.phone_number}</td>
                        <td>{user.userInfos.email}</td>
                        <td>{user.workData.company_name}</td>
                        <td>{user.workData.job_type}</td>
                        <td>{user.workData.experience}</td>
                    </tr>
                })}
            </tbody>
        </table>
    )
}

export default UserList
