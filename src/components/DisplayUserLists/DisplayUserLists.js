import axios from 'axios'
import { useEffect, useState } from 'react'
import UserList from './UserList'
import './DisplayUserLists.css'


const DisplayUserLists = () => {

  

  return (
    <div className='userList_wrapper bg'>
      <h1>User Lists</h1>
      <hr/>
      <br/>
      <UserList />
    </div>
  )
}

export default DisplayUserLists
