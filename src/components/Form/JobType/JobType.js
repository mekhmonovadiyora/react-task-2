import ButtonInside from '../Buttons/ButtonInside'
import axios from 'axios'
import { useState, useEffect } from "react"


const JobType = () => {

  let formValid = false

  // postUrl

  const postUrl = 'https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types'

  const [jobLabel, setJobLabel] = useState('')

  // onChange 

  const jobLabelChangeHandler = event => {
    setJobLabel(event.target.value)
  }

  // Validity

  const jobLabelValid = jobLabel.trim().length >= 2
  const [jobLabelTouched, setJobLabelTouched] = useState(false)
  const jobLabelTouchHandler = e => {
    setJobLabelTouched(true)
  }

  if (jobLabelValid) {
    formValid = true
  }

  // onClick

    const onClickHandler = e => {
        e.preventDefault()

        if (!formValid) {
          return
        }

        axios.post(postUrl, {
          label: jobLabel
        })

        setJobLabel('')
        setJobLabelTouched(false)

    }

  return (
    <fieldset className='bg add'>
      <h1>Job type</h1>
      <hr />
      <label>
        <p>Label</p>
        <input
          value={jobLabel}
          onChange={jobLabelChangeHandler}
          onBlur={jobLabelTouchHandler}
          className={`${!jobLabelValid && jobLabelTouched ? 'invalid' : ''}`}
        />
        {(!jobLabelValid && jobLabelTouched) && <span className="errorText">Please enter a valid job label</span>}
      </label>
      <div className='btns'>
        <button className='cancel'>Отменить</button>
        <button className='save' onClick={onClickHandler}>Сохранить</button>
      </div>
    </fieldset>
  )
}

export default JobType
