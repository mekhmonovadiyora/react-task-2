import { useState } from "react"


const ButtonOutside = () => {
    return (
        <div className='btns'>
            <button className='cancel'>Отменить</button>
            <button className='save'>Сохранить</button>
        </div>
    )
}

export default ButtonOutside
