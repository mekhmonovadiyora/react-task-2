
import { useState } from "react"

const ButtonInside = () => {

    const [clicked, setClicked] = useState(false)

    // onClick

    const onClickHandler = e => {
        e.preventDefault()
        setClicked(true)
    }

    return (
        <div className='btns'>
            <button className='cancel'>Отменить</button>
            <button className='save' onClick={onClickHandler}>Сохранить</button>
        </div>
    )
}

export default ButtonInside
