

import WorkDetails from './WorkDetails/WorkDetails'
import JobType from './JobType/JobType'


const FormTail = () => {
    return (
        <div className='workDetails_jobType'>
            <WorkDetails />
            <JobType />
        </div>
    )
}

export default FormTail
