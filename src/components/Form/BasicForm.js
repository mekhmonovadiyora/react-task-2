
import UserInfo from './UserInfo/UserInfo'
import FormTail from './FormTail'
import ButtonOutside from './Buttons/ButtonOutside'
import './Form.css'

const BasicForm = () => {

    let formValid = false

    const onSubmitHandler = e => {
        e.preventDefault()
        if (!formValid) {
            return
        }
        
    }

    return (
        <div className='form_wrapper' >
            <form onSubmit={onSubmitHandler} className="form" >
                <UserInfo />
                <FormTail />
            </form>
            <ButtonOutside disable={formValid}/>
        </div>
    )
}

export default BasicForm


