
import axios from "axios"
import { useState, useEffect } from "react"

const UserInfo = () => {

  // PostUrl

  const postUrl = 'https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/users'

  const [userInfos, setUserInfos] = useState({
    firstName: '',
    lastName: '',
    phone_number: '',
    email: '',
    dob: ''
  })

  // onChange

  const nameChangeHandler = (event) => {
    event.persist()
    setUserInfos(values => ({
      ...userInfos,
      firstName: event.target.value
    }))
  }

  const surnameChangeHandler = (event) => {
    event.persist()
    setUserInfos(values => ({
      ...userInfos,
      lastName: event.target.value
    }))
  }

  const phoneNumberChangeHandler = (event) => {
    setUserInfos(values => ({
      ...userInfos,
      phone_number: event.target.value
    }))
  }

  const emailChangeHandler = (event) => {
    setUserInfos(values => ({
      ...userInfos,
      email: event.target.value
    }))
  }

  const dateOfBChangeHandler = (event) => {
    setUserInfos(values => ({
      ...userInfos,
      dob: event.target.value
    }))
  }

  // Post-Get

  // useEffect(
  //   axios.post(postUrl, {
  //     user_infos: userInfos
  //   }).then(response => {
  //     console.log('response')
  //   })
  // )

  // Validity
  const nameValid = userInfos.firstName.trim().length >= 5
  const [nameTouched, setNameTouched] = useState(false)
  const nameTouchHandler = e => {
    setNameTouched(true)
  }
  const surnameValid = userInfos.lastName.trim().length >= 5
  const [surnameTouched, setSurnameTouched] = useState(false)
  const surnameTouchHandler = e => {
    setSurnameTouched(true)
  }
  let phoneno = /^\d{10}$/
  const phoneNValid = userInfos.phone_number.trim().match(phoneno)
  const [phoneNumberTouched, setPhoneNumberTouched] = useState(false)
  const phoneNumberTouchHandler = e => {
    setPhoneNumberTouched(true)
  }
  const emailValid = userInfos.email.trim().includes('@') && userInfos.email.trim().includes('.com')
  const [emailTouched, setEmailTouched] = useState(false)
  const emailTouchHandler = e => {
    setEmailTouched(true)
  }
  const dobValid = userInfos.dob.length > 0
  const [dobTouched, setDobTouched] = useState(false)
  const dobTouchHandler = e => {
    setDobTouched(true)
  }

  return (
    <fieldset className='bg user_info'>
      <h1>User's Info</h1>
      <hr />
      <label>
        <p>Name</p>
        <input
          type='text'
          name='name'
          value={userInfos.firstName}
          onChange={nameChangeHandler}
          onBlur={nameTouchHandler}
          className={`${!nameValid && nameTouched ? 'invalid' : ''}`}
        />
        {(!nameValid && nameTouched) && <span className="errorText">Please enter a valid first name</span>}
      </label>
      <label>
        <p>Surname</p>
        <input
          type='text'
          name='surname'
          value={userInfos.lastName}
          onChange={surnameChangeHandler}
          onBlur={surnameTouchHandler}
          className={`${!surnameValid && surnameTouched ? 'invalid' : ''}`}
        />
        {(!surnameValid && surnameTouched) && <span className="errorText">Please enter a valid phone number</span>}
      </label>
      <label>
        <p>Phone Number</p>
        <input
          type='text'
          name='phoneNumber'
          value={userInfos.phone_number}
          onChange={phoneNumberChangeHandler}
          onBlur={phoneNumberTouchHandler}
          className={`${!phoneNValid && phoneNumberTouched ? 'invalid' : ''}`}
        />
        {(!phoneNValid && phoneNumberTouched) && <span className="errorText">Please enter a valid phone number</span>}
      </label>
      <label>
        <p>Email</p>
        <input
          type="email"
          name='email'
          value={userInfos.email}
          onChange={emailChangeHandler}
          onBlur={emailTouchHandler}
          className={`${!emailValid && emailTouched ? 'invalid' : ''}`}
        />
        {(!emailValid && emailTouched) && <span className="errorText">Please enter a valid email address</span>}
      </label>
      <label>
        <p>Date of Birth</p>
        <input
          type="date"
          name='dateOfBirth'
          value={userInfos.dob}
          onChange={dateOfBChangeHandler}
          onBlur={dobTouchHandler}
          className={`${!dobValid && dobTouched ? 'invalid' : ''}`}
        />
        {(!dobValid && dobTouched) && <span className="errorText">Please enter a valid Date of Birth</span>}
      </label>
    </fieldset>

  )
}

export default UserInfo
