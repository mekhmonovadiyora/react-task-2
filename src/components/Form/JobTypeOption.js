

import React from 'react'

const JobTypeOption = (props) => {
    return (
        <>
            <option value={props.option}>{props.option}</option>
        </>
    )
}

export default JobTypeOption
