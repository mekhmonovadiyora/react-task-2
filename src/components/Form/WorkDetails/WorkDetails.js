import { useState, useEffect } from "react"
import JobTypeOption from "../JobTypeOption"
import axios from "axios"


const WorkDetails = () => {

  const postUrl = 'https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/users'
  const getUrl = 'https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types'

  const [workArea, setWorkArea] = useState({
    company_name: '',
    experience: 0,
    job_type: ''
  })

    // onChange

  const companyNameChangeHandler = (event) => {
    setWorkArea(values => ({
      ...workArea,
      company_name: event.target.value
    }))
  }

  const jobTypeChangeHandler = (event) => {
    setWorkArea(values => ({
      ...workArea,
      job_type: event.target.value
    }))
  }

  const experienceChangeHandler = (event) => {
    setWorkArea(values => ({
      ...workArea,
      experience: event.target.value
    }))
  }

  // Validity

  const companyNValid = workArea.company_name.trim().length >= 3
  const [companyNameTouched, setCompanyNameTouched] = useState(false)
  const companyNameTouchHandler = e => {
    setCompanyNameTouched(true)
  }
  const jobTypeValid = workArea.job_type !== 'DEFAULT'
  const [jobTypeTouched, setJobTypeTouched] = useState(false)
  const jobTypeTouchHandler = e => {
    setJobTypeTouched(true)
  }

  const experienceValid = workArea.experience !== 'DEFAULT'
  const [experienceTouched, setExperienceTouched] = useState(false)
  const experienceTouchHandler = e => {
    setExperienceTouched(true)
  }

  // onClick

    const [dataToSelect, setDataToSelect] = useState([])

    useEffect(()=> {
        axios.get(getUrl).then(response => {
            let newDataToSelect = []

            response.data.map(data => {
                newDataToSelect.push(data.label)
            })
            setDataToSelect(newDataToSelect)
        })
    }, [])

    // Post Work Details

  // useEffect(
  //   axios.post(postUrl, {
  //     work_area: workArea
  //   }).then(response => {
  //     console.log(response)
  //   }), [])

  return (
    <fieldset className='bg add'>
      <h1>Work Details</h1>
      <hr />
      <label>
        <p>Company Name</p>
        <input
          type="text"
          name='companyName'
          value={workArea.company_name}
          onChange={companyNameChangeHandler}
          onBlur={companyNameTouchHandler}
          className={`${!companyNValid && companyNameTouched ? 'invalid' : ''}`}
        />
        {(!companyNValid && companyNameTouched) && <span className="errorText">Please enter a valid Company Name</span>}      
      </label>
      <div className='workDetails'>
        <label>
          <p>Job Type</p>
          <select
            name='jobType'
            value={workArea.job_type}
            onChange={jobTypeChangeHandler}
          >
            <option value="DEFAULT">Choose your Job type</option>
            {dataToSelect.map(data =>
              <JobTypeOption key={data} option={data} />
            )}
          </select>
          {(!jobTypeValid && jobTypeTouched) && <span className="errorText">Please select a valid Job type</span>}      
        </label>
        <label>
          <p>Experience</p>
          <select
            name='expYear'
            value={workArea.experience}
            onChange={experienceChangeHandler}
          >
            <option value="DEFAULT">Choose your experience</option>
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
            <option value='4'>4</option>
            <option value='5'>5</option>
          </select>
          {(!experienceValid && experienceTouched) && <span className="errorText">Please enter a valid Experience year</span>}      
        </label>
      </div>
    </fieldset>
  )
}

export default WorkDetails
