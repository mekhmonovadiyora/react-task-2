// import axios from 'axios'
// import { useState, useEffect } from 'react'
// import JobTypeOption from './JobTypeOption'
// import './Form.css'


// const Form = (props) => {

//     let formValid = false
//     const [submitted, setSubmitted] = useState(false)
//     const [clicked, setClicked] = useState(false)

//     // POST link

//     const postUrl = 'https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/users'
//     const getUrl = 'https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/types'
//     // Data to POST

//     const [userInfos, setUserInfos] = useState({
//         firstName: '',
//         lastName: '',
//         phone_number: '',
//         email: '',
//         dob: ''
//     })

//     const [workArea, setWorkArea] = useState({
//         company_name: '',
//         experience: 0,
//         job_type: ''
//     })

//     // Data to Job Type Select

//     const [dataToSelect, setDataToSelect] = useState([])

//     useEffect(()=> {
//         axios.get(getUrl).then(response => {
//             let newDataToSelect = []

//             response.data.map(data => {
//                 newDataToSelect.push(data.label)
//             })
//             setDataToSelect(newDataToSelect)
//         })
//     }, [])

//     // Submit 

//     const onSubmitHandler = e => {
//         e.preventDefault()
//         setSubmitted(true)
//         console.log(userInfos, workArea)
//     }

//     // onClick

//     const onClickHandler = e => {
//         e.preventDefault()
//         setClicked(true)
//         console.log(workArea.job_type)
//     }

//     // onChange

//     const nameChangeHandler = (event) =>{
//         event.persist()
//         setUserInfos(values=>({
//             ...userInfos,
//             firstName: event.target.value
//         }))
//     }

//     const surnameChangeHandler = (event) => {
//         event.persist()
//         setUserInfos(values => ({
//             ...userInfos,
//             lastName: event.target.value
//         }))
//     }

//     const phoneNumberChangeHandler = (event) => {
//         setUserInfos(values => ({
//             ...userInfos,
//             phone_number: event.target.value
//         }))
//     }

//     const emailChangeHandler = (event) => {
//         setUserInfos(values => ({
//             ...userInfos,
//             email: event.target.value
//         }))
//     }

//     const dateOfBChangeHandler = (event) => {
//         setUserInfos(values => ({
//             ...userInfos,
//             dob: event.target.value
//         }))
//     }

//     const companyNameChangeHandler = (event) => {
//         setWorkArea(values => ({
//             ...workArea,
//             company_name: event.target.value
//         }))
//     }

//     const jobTypeChangeHandler = (event) => {
//         setWorkArea(values => ({
//             ...workArea,
//             job_type: event.target.value
//         }))
//     }

//     const experienceChangeHandler = (event) => {
//         setWorkArea(values => ({
//             ...workArea,
//             experience: event.target.value
//         }))
//     }

//     // Validity
//     const nameValid = userInfos.firstName.trim().length >= 5
//     const [nameTouched, setNameTouched] = useState(false)
//     const nameTouchHandler = e => {
//         setNameTouched(true)
//     }
//     const surnameValid = userInfos.lastName.trim().length >= 5
//     const [surnameTouched, setSurnameTouched] = useState(false)
//     const surnameTouchHandler = e => {
//         setSurnameTouched(true)
//     }
//     let phoneno = /^\d{10}$/
//     const phoneNValid = userInfos.phone_number.trim().match(phoneno)
//     const [phoneNumberTouched, setPhoneNumberTouched] = useState(false)
//     const phoneNumberTouchHandler = e => {
//         setPhoneNumberTouched(true)
//     }
//     const emailValid = userInfos.email.trim().includes('@') && userInfos.email.trim().includes('.com')
//     const [emailTouched, setEmailTouched] = useState(false)
//     const emailTouchHandler = e => {
//         setEmailTouched(true)
//     }
//     const dobValid = userInfos.dob.length > 0
//     const [dobTouched, setDobTouched] = useState(false)
//     const dobTouchHandler = e => {
//         setDobTouched(true)
//     }
//     const companyNValid = workArea.company_name.trim().length >= 3
//     const [companyNameTouched, setCompanyNameTouched] = useState(false)
//     const companyNameTouchHandler = e => {
//         setCompanyNameTouched(true)
//     }
//     const jobTypeValid = workArea.job_type.trim().length >= 3
//     const [jobTouched, setJobTouched] = useState(false)
//     const jobTouchHandler = e => {
//         setJobTouched(true)
//     }

//     return (
//         <form className="form_wrapper" onSubmit={onSubmitHandler}>
//             <fieldset className='bg user_info'>
//                 <h1>User's Info</h1>
//                 <hr />
//                 <label>
//                     <p>Name</p>
//                     <input
//                         type='text'
//                         name='name'
//                         value={userInfos.firstName}
//                         onChange={nameChangeHandler}
//                         onBlur={nameTouchHandler}
//                         className={`${!nameValid && nameTouched ? 'invalid' : ''}`}
//                     />
//                     { (!nameValid && nameTouched) && <span className="errorText">Please enter a valid first name</span>}
//                 </label>
//                 <label>
//                     <p>Surname</p>
//                     <input
//                         type='text'
//                         name='surname'
//                         value={userInfos.lastName}
//                         onChange={surnameChangeHandler}
//                         onBlur={surnameTouchHandler}
//                         className={`${!surnameValid && surnameTouched ? 'invalid' : ''}`}
//                     />
//                     { (!surnameValid && surnameTouched) && <span className="errorText">Please enter a valid phone number</span>}
//                 </label>
//                 <label>
//                     <p>Phone Number</p>
//                     <input
//                         type='text'
//                         name='phoneNumber'
//                         value={userInfos.phone_number}
//                         onChange={phoneNumberChangeHandler}
//                         onBlur={phoneNumberTouchHandler}
//                         className={`${!phoneNValid && phoneNumberTouched ? 'invalid' : ''}`}
//                     />
//                     { (!phoneNValid && phoneNumberTouched) && <span className="errorText">Please enter a valid phone number</span>}
//                 </label>
//                 <label>
//                     <p>Email</p>
//                     <input
//                         type="email"
//                         name='email'
//                         value={userInfos.email}
//                         onChange={emailChangeHandler}
//                         onBlur={emailTouchHandler}
//                         className={`${!emailValid && emailTouched ? 'invalid' : ''}`}
//                     />
//                     { (!emailValid && emailTouched) && <span className="errorText">Please enter a valid email address</span>}
//                 </label>
//                 <label>
//                     <p>Date of Birth</p>
//                     <input
//                         type="date"
//                         name='dateOfBirth'
//                         value={userInfos.dob}
//                         onChange={dateOfBChangeHandler}
//                         onBlur={dobTouchHandler}
//                         className={`${!dobValid && dobTouched ? 'invalid' : ''}`}
//                     />
//                     {(!dobValid && dobTouched) && <span className="errorText">Please enter a valid Date of Birth</span>}
//                 </label>
//             </fieldset>

//             <div className='workDetails_jobType'>
//                 <fieldset className='bg add'>
//                     <h1>Work Details</h1>
//                     <hr />
//                     <label>
//                         <p>Company Name</p>
//                         <input
//                             type="text"
//                             name='companyName'
//                             value={workArea.company_name}
//                             onChange={companyNameChangeHandler}
//                             onBlur={companyNameTouchHandler}
//                             className={`${!companyNValid && companyNameTouched ? 'invalid' : ''}`}
//                         />
//                         { (!companyNValid && companyNameTouched) && <span className="errorText">Please enter a valid Company Name</span>}
//                     </label>
//                     <div className='workDetails'>
//                         <label>
//                             <p>Job Type</p>
//                             <select 
//                                 name='jobType'
//                                 value={workArea.job_type}
//                                 onChange={jobTypeChangeHandler}
//                             >
//                                 <option value="DEFAULT">Choose your Job type</option>
//                                 {dataToSelect.map(data => 
//                                     <JobTypeOption key={data} option={data} />
//                                 )}
//                             </select>
//                         </label>
//                         <label>
//                             <p>Experience</p>   
//                             <select 
//                                 name='expYear'
//                                 value={workArea.experience}
//                                 onChange={experienceChangeHandler}
//                             >
//                                 <option value="DEFAULT">Choose your experience</option>
//                                 <option value='1'>1</option>
//                                 <option value='2'>2</option>
//                                 <option value='3'>3</option>
//                                 <option value='4'>4</option>
//                                 <option value='5'>5</option>
//                             </select>
//                         </label>
//                     </div>
//                 </fieldset>
//                 <fieldset className='bg add'>
//                     <h1>Job type</h1>
//                     <hr />
//                     <label>
//                         <p>Label</p>
//                         <input 
//                             value={workArea.job_type}
//                             onChange={jobTypeChangeHandler}
//                             onBlur={jobTouchHandler}
//                             className={`${!jobTypeValid && jobTouched ? 'invalid' : ''}`}
//                         />
//                         { (!jobTypeValid && jobTouched) && <span className="errorText">Please enter a valid job label</span>}
//                     </label>
//                     <div className='btns'>
//                         <button className='cancel'>Отменить</button>
//                         <button className='save' onClick={onClickHandler}>Сохранить</button>
//                     </div>
//                 </fieldset>
//                 <div className='btns'>
//                     <button className='cancel'>Отменить</button>
//                     <button className='save'>Сохранить</button>
//                 </div>
//             </div>

//         </form>
//     )
// }

// export default Form
