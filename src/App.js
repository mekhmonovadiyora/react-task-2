
// import Form from './components/Form/Form'
import BasicForm from './components/Form/BasicForm'
import DisplayJobType from './components/DisplayJobType/DisplayJobType'
import DisplayUserLists from './components/DisplayUserLists/DisplayUserLists'
import './App.css'

const App = () => {
  return (
    <div className='wrapper'>
      <BasicForm className='bg'/>
      <DisplayUserLists className='bg'/>
      <DisplayJobType className='bg'/>
    </div>
  )
}

export default App
